app.controller('CadastroController', function($scope, $cordovaImagePicker, $state,$cordovaToast, LoginService,daoService,$ionicNavBarDelegate) {

  $scope.feed = {};
  $scope.feed.foto = "img/sem-foto.jpg";

  $scope.saveFeed = function() {
    var userObj = JSON.parse(LoginService.getUser());
    $scope.feed.author = userObj.name;
    daoService.addFeed($scope.feed);
    $scope.feed = {};
    $state.go('logado.feed');
    $cordovaToast
    .show('Post publicado', 'short', 'bottom').then(function(success){}, function(error){});

  }

  $scope.pickImage = function() {

    var options = {
      maximumImagesCount: 1,
      width: 800,
      height: 800,
      quality: 1
    };

    $cordovaImagePicker.getPictures(options)
      .then(function(results) {
        for (var i = 0; i < results.length; i++) {
                $scope.feed.foto = results[i];
                window.plugins.Base64.encodeFile($scope.collection.selectedImage, function(base64) {
                $scope.feed.foto = base64;
          });
        }
      }, function(error) {
            console.log(error);
      });
  }

});
