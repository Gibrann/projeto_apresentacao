app.factory('daoService', function() {

  var dao = {};
  var self = this;
  self.feeds = [
    {
      "id": 1,
      "title": "A Tormenta das espadas",
      "foto": "img/a-tormenta-das-espadas.jpg",
      "comment": "Acabei de ler e que livro em ? hahaha Martin é genial! Intrigas a todo momento fora as caras e bocas que vc faz lendo.",
      "author": "Gibran Tavares"
    }, {
      "id": 2,
      "title": "A Guerra dos Tronos",
      "foto": "img/guerra-dos-tronos.jpg",
      "comment": "Que livro gigante pessoal! Mas não se assustem, vale cada palavra impressa nesse montão de páginas.",
      "author": "Gibran Tavares"
    }, {
      "id": 3,
      "title": "O Festim dos Corvos",
      "foto": "img/o-festim-dos-corvos.jpg",
      "comment": "Eu achei o livro bom, e esperava uma pausa, o Tomenta de Espadas chega a sufocar o leitor de euforia.",
      "author": "Gibran Tavares"
    }, {
      "id": 4,
      "title": "A Fúria dos Reis",
      "foto": "img/a-furia-dos-reis.jpg",
      "comment": "É a primeira série que leio e que não tenho a certeza de que tudo terminará bem.",
      "author": "Gibran Tavares"
    }, {
      "id": 5,
      "title": "A Fúria dos Dragões",
      "foto": "img/a-danca-dos-dragoes.jpg",
      "comment": "Não foi uma leitura tão decepcionante quanto O Festim dos Corvos, mas ainda assim não supera A Tormenta de Espadas.",
      "author": "Gibran Tavares"
    }
  ];

  dao.getFeeds = function() {
      return self.feeds;
  }

  dao.addFeed = function(feed) {
      self.feeds.push(feed);
  }

  return dao;

});
