(function(){
  app.service('LoginService', function($q, $state,$cordovaDialogs){

      var USER = 'user';
      var self = this;
      self.isLogado = false;
      self.resolve = false;
      self.usuario = {};
      var servico = {};

      self.users =  [{"name":"Pedro", "email":"pedro@mail.com", "password":"123"},
                    {"name":"Gibran", "email":"gibran@mail.com", "password":"456"}];

      servico.logout = function(){
            $cordovaDialogs.confirm('Deseja sair do sistema?', 'My Favorite Book', ['Cancelar','Confirmar'])
              .then(function(buttonIndex) {
               var btnIndex = buttonIndex;
               if(btnIndex == 2){
                 window.localStorage.removeItem(USER);
                 self.isLogado = false;
                 $state.go('login');
               }
            });
      }

      servico.getAuthStatus = function(){
        return self.isLogado;
      }

      function setUser(usuario){
         self.usuario = usuario;
      }

      servico.getUser = function(){
        return window.localStorage.getItem(USER);
      }

      function storeUser(usuario){
        console.log(usuario);
        window.localStorage.setItem(USER, angular.toJson(usuario));
        console.log(JSON.parse(window.localStorage.getItem(USER)).email);
        setUser(usuario);
      }

      servico.login = function(email, senha){
        return $q(function(resolve, reject){
            self.users.forEach(function(usuario){
                if(email == usuario.email && senha == usuario.password){
                      storeUser(usuario);
                      self.isLogado = true;
                      resolve("Login success");
                      self.resolve = true;
                  }
            });

            if(self.resolve == false){
                reject('Email/Senha inválida!');
            }

            self.resolve = false;

        });

      }

      return servico;
  });


})();
