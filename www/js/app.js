var app = angular.module('starter', ['ionic', 'ngCordova'])

.run(function($ionicPlatform,$rootScope,$ionicNavBarDelegate,LoginService,$state) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });

  $rootScope.$on('$stateChangeStart', function (event, toState, toStateParams, fromState, fromStateParams) {

   console.log("Changing state to :");
   console.log(toState);
     if(toState.name.indexOf('logado') !== -1 ) {

       if(!LoginService.getAuthStatus()) {
             event.preventDefault();
             $state.go("login");
       }

     }
   });

})

.config(function($stateProvider, $urlRouterProvider){
    $stateProvider

    .state('login', {
          url: '/login',
          templateUrl: 'templates/login.html',
          controller: 'LoginController'
      })

      .state('logado', {
          url: '/logado',
          abstract: true,
          templateUrl: 'templates/sidemenu.html',
          controller: 'SideMenuController'
      })
      .state('logado.feed', {
          url: '/feed',
          views : {
              'menuContent' : {
                templateUrl: 'templates/feed-books.html',
                  controller: 'FeedController'
              }
          }
      })
      .state('logado.detalhe', {
          url: '/detalhe/:feedId',
          views : {
            'menuContent': {
              templateUrl: 'templates/feed-detail.html',
              controller: 'FeedDetailController'
            }
          }
      })
      .state('logado.cadastro-feed', {
          url: '/cadastro-feed',
          views: {
            'menuContent':{
              templateUrl: 'templates/feed-cadastro.html',
              controller: 'CadastroController'
            }
          }
      });

      $urlRouterProvider.otherwise("/login");

});
