app.controller('LoginController', function($scope, $state, LoginService, $cordovaDialogs) {

  if(LoginService.getUser()){
      $state.go('logado.feed');
  }

  $scope.user = {};

  $scope.logar = function() {
    console.log('clicou');
    LoginService.login($scope.user.login, $scope.user.password).then(function(data){
        $scope.user = {};
        $state.go('logado.feed');
    }, function(erro){
        $cordovaDialogs.alert('Usuário/Senha inválida', 'Erro de autenticação', 'Ok').then(function(){},function(){});
    });
  }

});
