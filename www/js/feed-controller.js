app.controller('FeedController', function($scope,$state, LoginService,$rootScope, $cordovaDialogs,daoService,$q){

    $rootScope.feeds = [];

    console.log($state.get());

    $scope.getFeeds = function(){
      $rootScope.feeds = daoService.getFeeds();
      $scope.$broadcast('scroll.refreshComplete');
    }

    $scope.detalhe = function(feed){
      $state.go('logado.detalhe',{feedId: feed});
    }

    $scope.addView = function(){
      $state.go('logado.cadastro-feed');
    }

    $scope.getFeeds();

    $scope.doRefresh  = function(){
      $scope.getFeeds();
    }

});
